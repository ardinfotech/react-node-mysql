##Installation##

User can clone the source code from git clone https://ardinfotech@bitbucket.org/ardinfotech/react-node-mysql.git or from
https://github.com/ardinfotech/node_react_user_listing.git

##Setup##

You can change the database configuration in the server.js which is present in the backend folder

"backend/server.js"

Below is the code snippet where you can replace the basic details with yours

const db = mysql.createPool({
host: "localhost",
user: "root",
password: "root",
database: "userListing",
});

##Database and tables setup

Run the below queries

1. CREATE DATABASE userListing;

2. CREATE TABLE `userListing`.`users` (
   `id` INT(11) NOT NULL AUTO_INCREMENT,
   `firstName` VARCHAR(45) NOT NULL,
   `lastName` VARCHAR(45) NOT NULL,
   `avatar` VARCHAR(255) NULL,
   PRIMARY KEY (`id`));

3. insert into users (firstName,lastName,avatar) values("ajay","sharma","/images/user.png"),("rajesh","sharma","/images/user.png"),("vijay","sharma","/images/user.png");

4. CREATE TABLE `userListing`.`userfriends` (
   `id` INT(11) NOT NULL AUTO_INCREMENT,
   `userid` INT(11) NOT NULL,
   `friendid` INT(11) NOT NULL,
   PRIMARY KEY (`id`));

5. insert into userfriends (userid,friendid) values(1,2),(1,3);

6. insert into userfriends (userid,friendid) values(2,1),(2,4);
