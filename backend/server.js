import express from "express";
import dotenv from "dotenv";
import mysql from "mysql";

dotenv.config();
const app = express();
const db = mysql.createPool({
  host: "localhost",
  user: "root",
  password: "root",
  database: "userListing",
});
app.get("/", (req, res) => {
  res.send("API is running");
});

app.get("/api/users", (req, res) => {
  const selectSql = "select * from users";
  db.query(selectSql, (err, result) => {
    res.send(result);
  });
});

app.get("/api/userfriends/:id", (req, res) => {
  const selectSql =
    "select fu.id,fu.firstName,fu.lastName,fu.avatar from userfriends inner Join users on users.id = userfriends.userid inner join users as fu on fu.id = userfriends.friendid  where userfriends.userid = " +
    req.params.id;
  db.query(selectSql, (err, result) => {
    res.send(result);
  });
});
const PORT = process.env.PORT || 5000;
app.listen(
  PORT,
  console.log(`server running in ${process.env.NODE_ENV} port ${PORT}`)
);
