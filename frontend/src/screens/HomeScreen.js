import React, { useState, useEffect } from "react";
import Users from "../components/Users";
import Pagination from "../components/Pagination";
import { Row, Col, Table } from "react-bootstrap";
import axios from "axios";
const HomeScreen = () => {
  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState([false]);
  const [currentPage, setCurrentPage] = useState([1]);
  const [postsPerPage] = useState([12]);

  useEffect(() => {
    const fetchUsers = async () => {
      setLoading(true);
      const { data } = await axios.get("/api/users");

      setUsers(data);
      setLoading(false);
    };

    fetchUsers();
  }, []);

  //Get current page
  const indexLast = currentPage * postsPerPage;
  const indexFirst = indexLast - postsPerPage;
  const currentPosts = users.slice(indexFirst, indexLast);
  //paginate
  const paginate = (pageNumber) => setCurrentPage(pageNumber);
  return (
    <>
      <Table striped bordered hover variant="dark">
        <thead>
          <tr>
            <th>#</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Avatar</th>
            <th>View Friends</th>
          </tr>
        </thead>
        <tbody>
          {currentPosts.map((user) => (
            <Users user={user} loading={loading}></Users>
          ))}
        </tbody>
      </Table>
      {/* <Pagination
        postsPerPage={postsPerPage}
        totalPosts={users.length}
        paginate={2}
      /> */}
    </>
  );
};

export default HomeScreen;
