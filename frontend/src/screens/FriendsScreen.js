import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Table } from "react-bootstrap";
import axios from "axios";
import Users from "../components/Users";

const FriendsScreen = ({ match }) => {
  const [users, setUserFriends] = useState([]);
  useEffect(() => {
    const fetchUserFriends = async () => {
      const { data } = await axios.get(`/api/userfriends/${match.params.id}`);
      setUserFriends(data);
    };

    fetchUserFriends();
  }, [match]);
  return (
    <>
      <Link className="btn btn-dark my-3" to="/">
        Go Back
      </Link>
      <Table striped bordered hover variant="dark">
        <thead>
          <tr>
            <th>#</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Avatar</th>
            <th>View Friends</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user) => (
            <Users user={user}></Users>
          ))}
        </tbody>
      </Table>
    </>
  );
};

export default FriendsScreen;
