//import logo from './logo.svg';
//import './App.css';
import { Component } from "react";
import Header from "./components/Header";
import Footer from "./components/Footer";
import HomeScreen from "./screens/HomeScreen";
import FriendsScreen from "./screens/FriendsScreen";
import { Container } from "react-bootstrap";
import { BrowserRouter as Router, Route } from "react-router-dom";

const App = () => {
  return (
    <Router>
      <Header />

      <main className="py-3">
        <Container>
          <Route path="/" component={HomeScreen} exact />
          <Route path="/userfriends/:id" component={FriendsScreen} />
        </Container>
      </main>
      <Footer />
    </Router>
  );
};

export default App;
