import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

const Users = ({ user, loading }) => {
  if (loading) {
    return <h2>Loading....</h2>;
  }
  return (
    <tr>
      <td>{user.id}</td>
      <td>{user.firstName}</td>
      <td>{user.lastName}</td>
      <td>
        <img src={user.avatar} alt="avatar" />
      </td>
      <td>
        <Link to={`/userfriends/${user.id}`}>View</Link>
      </td>
    </tr>
  );
};

export default Users;
